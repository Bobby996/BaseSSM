package com.bobby.controller;

import com.bobby.domain.Account;
import com.bobby.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.jws.WebParam;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Classname AccountController
 * @Description TODO
 * @Date 2020/4/28 8:53
 * @Created wenjunpei
 */
@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @RequestMapping("/findAll.do")
    public Map<String,Object> findAll() {
        Map<String,Object> map = new HashMap<>();
        map.put("data",accountService.findAll());
        return map;
    }

    @RequestMapping("/findById.do")
    public ModelAndView findById(@RequestParam("id") Integer id) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("list",accountService.findById(id));
        modelAndView.setViewName("list");
        return modelAndView;
    }
}
