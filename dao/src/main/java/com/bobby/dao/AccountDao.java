package com.bobby.dao;

import com.bobby.domain.Account;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Classname AccountDao
 * @Description TODO
 * @Date 2020/4/27 19:51
 * @Created wenjunpei
 */
public interface AccountDao {

    @Select("select * from account")
    List<Account> findAll();

    Account findById(Integer id);
}
