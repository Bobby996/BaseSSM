package com.bobby.service.impl;

import com.bobby.domain.Account;
import com.bobby.dao.AccountDao;
import com.bobby.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Classname AccountServiceImpl
 * @Description TODO
 * @Date 2020/4/27 19:58
 * @Created wenjunpei
 */
@Service
@Transactional
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountDao accountDao;

    @Override
    public List<Account> findAll() {
        return accountDao.findAll();
    }

    @Override
    public Account findById(Integer id) {
        return accountDao.findById(id);
    }
}
