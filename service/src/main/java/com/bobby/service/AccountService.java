package com.bobby.service;

import com.bobby.domain.Account;

import java.util.List;

/**
 * @Classname AccountService
 * @Description TODO
 * @Date 2020/4/27 19:54
 * @Created wenjunpei
 */
public interface AccountService {

    List<Account> findAll();

    Account findById(Integer id);
}
